package ZFS::Pair;

use strict;
use warnings;

use ZFS::Host;
use List::Compare;

sub new {
  my $proto = shift;
  my $class = ref($proto) || $proto;
  my $self = {
    'srcHost' => undef,
    'destHost' => undef,
    'srcHostname' => shift,
    'destHostname' => shift,
    'zfsName' => undef,
    'childrenToIgnore' => undef,
    'mbufferconf' => {
# Trying PV
	'-B'=>'512M',
# buffer is a bit old and doesn't like big numbers
      #'-s' => '128k', 
      #'-m' => '512M', 
# http://manpages.ubuntu.com/manpages/wily/man1/mbuffer.1.html says
      #'-A' => '/bin/false',
# should make mbuffer quieter
# But that makes mbuffer think that there's an autoloader...
# So we try with buffer not mbuffer; that doesn't accept the -q flag
      #'-q' => '',  
    },
		'commonBackupTag' => 'commonbackup',
		'dryrun' => undef,
  };
  bless($self, $class);
	if(defined $self->{'srcHostname'}) { $self->setupSrc; }
	if(defined $self->{'destHostname'}) { $self->setupDest; }
  return $self;
}

sub zfsName {
  my $self=shift;
  if(@_) { 
    $self->{'zfsName'} = shift; 
    $self->{'srcHost'}->zfsName($self->zfsName);
    $self->{'destHost'}->zfsName($self->zfsName);
  }
  return $self->{'zfsName'};
}

# return a list of all children of $self->zfsName, but ignoring anything on the
# $self->childrenToIgnore list (and any child ZFSs of members of that list)

sub getChildFilesystems {
  my $self=shift;	
  my $parent = shift;
	if(! defined($parent)) { $parent=$self->zfsName; }
  my $cmd="/sbin/zfs list -H -oname -r $parent";
  my @children=$self->{'srcHost'}->runCmd($cmd,"isreadonly");
  chomp(@children);
  if(defined $self->childrenToIgnore) {
    foreach my $ignore (@{$self->childrenToIgnore}) {
# need to be careful with anchors here; e.g. if we want to ignore data/group/spri and all descendents thereof,
# but not to ignore data/group/spring
# ignore the item itself..   
      @children=grep ! /^$ignore$/, @children;
# and any children any of it (i.e. any ZFS starting $ignore/)
      @children=grep ! /^$ignore\//, @children;
    } 
  }
  return(@children);
}

sub srcHost {
  my $self=shift;
  return $self->{'srcHost'};
}

sub destHost {
  my $self=shift;
  return $self->{'destHost'};
}

sub srcHostname {
  my $self = shift;
  if(@_) { 
    $self->{'srcHostname'} = shift; 
    $self->setupSrc;
  }
  return $self->{'srcHostname'};
}

sub destHostname {
  my $self = shift;
  if(@_) { 
    $self->{'destHostname'} = shift; 
    $self->setupDest;
  }
  return $self->{'destHostname'};
}

sub commonBackupTag {
	my $self = shift;
	if(@_) { $self->{'commonBackupTag'} = shift; }
	return $self->{'commonBackupTag'};
}

sub setupSrc {
  my $self=shift;
  $self->{'srcHost'}=ZFS::Host->new();
  $self->{'srcHost'}->hostname($self->srcHostname);
  $self->{'srcHost'}->zfsName($self->zfsName);
  $self->{'srcHost'}->type("src");
}

sub setupDest {
  my $self=shift;
  $self->{'destHost'}=ZFS::Host->new();
  $self->{'destHost'}->hostname($self->destHostname);
  $self->{'destHost'}->zfsName($self->zfsName);
  $self->{'destHost'}->type("dst");
}

sub dryrun {
	my $self=shift;
	if(@_) { $self->{'dryrun'} = shift; }
	return $self->{'dryrun'};
}

sub setDryrun {
	my $self=shift;
	$self->dryrun("yes");
	$self->srcHost->setDryrun;
	$self->destHost->setDryrun;
}

sub childrenToIgnore {
  my $self=shift;
  if(@_) { $self->{'childrenToIgnore'} = shift; }
  return $self->{'childrenToIgnore'};
}

sub findCommonSnapshots {
  my $self=shift;
	my $listall=shift;
  my @srcSnaps=$self->srcHost->getSnapshots($listall);
  my @destSnaps=$self->destHost->getSnapshots($listall);
  my $lc=List::Compare->new(\@srcSnaps, \@destSnaps);
  return $lc->get_intersection;
}

sub findLastCommonSnapshot {
  my $self=shift;
	my $listall=shift;
  my @commonSnaps=$self->findCommonSnapshots($listall);
  while(my $candidate=pop(@commonSnaps)) {
    if($self->{'srcHost'}->zfsExists($candidate) and $self->{'destHost'}->zfsExists($candidate)) {
      # print "Last Common Snapshot is $candidate\n";
      return $candidate;
    }
  }
}

sub updateCommonBackupTag {
	my $self=shift;
	my $tag=shift;
	if(!defined $tag) { $tag=$self->commonBackupTag; }

	# first, note which snapshots have the tag
	my @taggedAtSrc=$self->srcHost->getHeldSnapshotsByTag($tag);
	my @taggedAtDest=$self->destHost->getHeldSnapshotsByTag($tag);

	# then tag the most recent common snapshot
	my $mostrecent=$self->findLastCommonSnapshot;
	$self->srcHost->holdSnapshot($tag,$mostrecent);
	$self->destHost->holdSnapshot($tag,$mostrecent);

	# finally, release the old ones whilst making sure we don't
	# release the latest one
	foreach my $snap (@taggedAtSrc) {
		$self->srcHost->releaseSnapshot($tag,$snap) unless $snap eq $mostrecent;
	}
	foreach my $snap (@taggedAtDest) {
		$self->destHost->releaseSnapshot($tag,$snap) unless $snap eq $mostrecent;
	}
}

sub backupAndSendIncremental {
  my $self=shift;
  my $lastcommon=$self->findLastCommonSnapshot;
  if($lastcommon) {
    my $newName=$self->srcHost->takeSnapshot; 
    my $cmd;
    if($self->mbufferCmd) {
      $cmd="/sbin/zfs send -i $lastcommon $newName |".$self->mbufferCmd."| ssh $self->{'destHostname'} '". $self->mbufferCmd." | /sbin/zfs recv $self->{'zfsName'}'";
    } else {
      $cmd="/sbin/zfs send -i $lastcommon $newName | ssh $self->{'destHostname'} /sbin/zfs recv $self->{'zfsName'}";
    }
    $self->srcHost->runCmd($cmd);
  } else {
    print "no common snapshots, bootstrapping\n";
    $self->bootstrap;
  }
}

sub sendIncrementals {
	my $self=shift;
	my $lastcommon=$self->findLastCommonSnapshot;
	if($lastcommon) {
		my $mostrecent=$self->srcHost->getLastSnapshotFullName;
		if($mostrecent eq $lastcommon) { 
			return; 
		} else {
			my $cmd;
			if($self->mbufferCmd) {
				$cmd="/sbin/zfs send -I $lastcommon $mostrecent |".$self->mbufferCmd."| ssh $self->{'destHostname'} '". $self->mbufferCmd." | /sbin/zfs recv $self->{'zfsName'}'";
			} else {
				$cmd="/sbin/zfs send -I $lastcommon $mostrecent | ssh $self->{'destHostname'} /sbin/zfs recv $self->{'zfsName'}";
			}
			$self->srcHost->runCmd($cmd);
		}
	} else {
		print "No common snapshot found\n";
		$self->bootstrap;
	}
}

# danger!! this will destroy data!!
sub rollbackToLastCommon {
	my $self=shift;
	my $dst=$self->destHost;
	my $lastcommon=$self->findLastCommonSnapshot;
	my $lastCommonTime=$dst->getZFSCreationTime($lastcommon);
	my @newersnapshots=$dst->getSnapshotsAfter($lastCommonTime,"listall");
	my $holds={};
	foreach my $snap(@newersnapshots) {
		$holds->{$snap}=[$dst->getHoldsOnSnapshot($snap)];
	}
	if($holds) {
		foreach my $snap(keys(%{$holds})) {
			foreach my $tag(@{$holds->{$snap}}) {
				my $snapcreation=$dst->getZFSCreationTime($dst->sanitiseSnapshotName($snap));
				if($snapcreation>$lastCommonTime) {
					$dst->releaseSnapshot($tag,$snap);
				}
			}
		}
	}
	$self->destHost->rollback($lastcommon);
}

sub bootstrap {
  my $self=shift;
  if($self->destHost->zfsExists($self->zfsName)) {
    print "$self->{'zfsName'} has no common snapshots but already exists on destination $self->{'destHostname'}\n";
    print "Fix this yourself! (possibly by running \"zfs rename $self->{'zfsName'} $self->{'zfsName'}-ARCHIVED-".time."\")\n"; 
    return 1;
  }

  $self->srcHost->takeSnapshot;
  my $newName=$self->srcHost->getLastSnapshotFullName; 
  my $cmd;
  if($self->mbufferCmd) {
    $cmd="/sbin/zfs send $newName |".$self->mbufferCmd."| ssh $self->{'destHostname'} '".$self->mbufferCmd." | /sbin/zfs recv -u $self->{'zfsName'}'";
  } else {
    $cmd="/sbin/zfs send $newName | ssh $self->{'destHostname'} /sbin/zfs recv -u $self->{'zfsName'}";
  }
  $self->srcHost->runCmd($cmd);
  $self->destHost->mount;
  $self->destHost->makeReadonly;
}

sub holdLastCommon {
	my $self=shift;
	my $tag=shift;
	my $common=$self->findLastCommonSnapshot;
	if(! defined $common) { 
		print "Couldn't find a common snapshot for $self->zfsName\n";
		return;
	}
  $self->srcHost->holdSnapshot($tag,$common);
	$self->destHost->holdSnapshot($tag,$common);
}

sub mbufferCmd {
  my $self=shift;
  my $cmd="";
  if(defined($self->{'mbufferconf'})) {
    $cmd="pv ";
    while((my $k, my $v) = each %{$self->{'mbufferconf'}}) {
      $cmd.="$k $v ";
    }
  }
  return $cmd;
}

1;
