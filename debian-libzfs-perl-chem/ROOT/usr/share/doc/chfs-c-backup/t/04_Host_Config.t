#!/usr/bin/perl -I/usr/local/lib/perl5/site_perl/
$|=1;
use strict;
use warnings;
use POSIX;
use ZFS::Host;
use ZFS::Config;

use Test::More tests => 6;
use Data::Dumper;

my $testhost='splot3.ch.private.cam.ac.uk';
my $zfsname="zpool1/test$$";

my $host=ZFS::Host->new();
$host->hostname($testhost);
print "Testing with zfs $zfsname on $testhost\n";

$host->zfsName($zfsname);

# Config.pm expects to read a config file. So, we'll embed one
# here as a heredoc, write it out, and read it back in again..

# TODO: refactor Config.pm so that src and dst hosts are optional.
# For now, though, we'll set them even though we're not going to use them

my $configdata=<<'CONFIG';

$main::src="splot3.ch.private.cam.ac.uk";
$main::dest="splot3.ch.private.cam.ac.uk";

my $hour=3600;
my $day=24*$hour;
my $week=7*$day;
my $month=4*$week;

%main::archivepolicy=(
  'hourly'=>{age=>$day,count=>10},
  'daily'=>{age=>$week,count=>7},
  'weekly'=>{age=>$month,count=>4},
  'monthly'=>{age=>6*$month,count=>6},
  );

1;

CONFIG

my $conffile;
do {
	$conffile=tmpnam();
} until sysopen(CONFFILE, $conffile, O_RDWR|O_CREAT|O_EXCL, 0666);

print CONFFILE $configdata;
close(CONFFILE);

# test Config.pm
my $config=ZFS::Config->new($conffile);
isa_ok($config, 'ZFS::Config', '$config');
my @expected_types=('hourly', 'daily', 'weekly', 'monthly');
my @types=$config->getArchiveTypes;

is_deeply([ sort @types], [sort @expected_types], 'getArchiveTypes as expected');

cmp_ok($config->ageOfArchiveType('hourly'), '==', 3600*24, 'ageOfArchiveType ok');
cmp_ok($config->numberOfArchiveType('daily'), '==', 7, 'numberOfArchiveType ok');

# test Host's idea of archive config

$host->archiveconfig($config);
isa_ok($host->archiveconfig, 'ZFS::Config', '$host->archiveconfig');
@types=$host->getArchiveTypes;
is_deeply([ sort @types], [sort @expected_types], 'host\'s getArchiveTypes as expected');
