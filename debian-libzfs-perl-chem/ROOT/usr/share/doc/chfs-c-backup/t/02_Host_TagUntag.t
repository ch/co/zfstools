#!/usr/bin/perl -I/usr/local/lib/perl5/site_perl/
$|=1;
use strict;
use warnings;

use ZFS::Host;

use Test::More tests => 26;
use Data::Dumper;

my $testhost='splot3.ch.private.cam.ac.uk';
my $zfsname="zpool1/test$$";

my $host=ZFS::Host->new();
$host->hostname($testhost);
print "Testing with zfs $zfsname on $testhost\n";

$host->zfsName($zfsname);
$host->createZFS($zfsname);
my $snapshot1=$host->takeSnapshot;
my $tag='test';
cmp_ok($host->snapshotHasTag($tag, $snapshot1), '==', 0, 'newly-created snapshot has no tag');
$host->holdSnapshot($tag, $snapshot1);
cmp_ok($host->snapshotHasTag($tag, $snapshot1), '==', 1, 'tagging succeeded');
my @holds=$host->getHoldsOnSnapshot($snapshot1);
isa_ok(\@holds, 'ARRAY', 'getHoldsOnSnapshot returns type');
my @expected_holds=($tag);
is_deeply(\@holds, \@expected_holds, 'getHoldsOnSnapshot - one snapshot, one tag');
$host->releaseSnapshot($tag, $snapshot1);
cmp_ok($host->snapshotHasTag($tag, $snapshot1), '==', 0, 'tag released');

# ensure that the second snapshot is taken at least a second after the first, so that the names are different
sleep 1;
my $snapshot2=$host->takeSnapshot;
$host->holdSnapshot($tag, $snapshot2);
my $expectedtags={};
$snapshot1 =~ s/.*@//;
$snapshot2 =~ s/.*@//;
$expectedtags->{$snapshot1}=[];
$expectedtags->{$snapshot2}=[$tag];
my $tagarray=$host->getArrayOfTagInfo;

isa_ok($tagarray, 'HASH');
isa_ok($tagarray->{$snapshot1}, 'ARRAY');
is_deeply(\$tagarray, \$expectedtags, 'getArrayOfTagInfo returns expected structure');

# test counting held snapshots
cmp_ok($host->countSnapshotsWithTag($tag), '==', 1, 'one snapshot with test tag');
cmp_ok($host->countSnapshotsWithTag('faketag'), '==', 0, 'no snapshots with faketag tag');

# test finding newest snapshot with tag
cmp_ok($host->newestSnapshotWithTag($tag), '==', $snapshot2, 'newestSnapshotWithTag with only one tag');
$host->holdSnapshot($tag, $snapshot1);
cmp_ok($host->newestSnapshotWithTag($tag), '==', $snapshot2, 'newestSnapshotWithTag - two snapshots with tag');
my $tag2='test2';
$host->holdSnapshot($tag2, $snapshot1);
cmp_ok($host->newestSnapshotWithTag($tag2), '==', $snapshot1, 'newestSnapshotWithTag - one snapshot with two tag');
$host->releaseSnapshot($tag, $snapshot2);
$host->releaseSnapshot($tag, $snapshot1);
$host->releaseSnapshot($tag2, $snapshot1);

# test getValidatedSnapshots function
sleep 1;
my $snapshot3=$host->takeSnapshot;
my @snapshot_list=$host->getValidatedSnapshots;
isa_ok(\@snapshot_list, 'ARRAY');
my $snapshot1_timestamp=$snapshot1;
# convert the bit after the @ to a full ZFS name
$snapshot1=$host->sanitiseSnapshotName($snapshot1);
is($snapshot1, $zfsname.'@'.$snapshot1_timestamp, 'sanitiseSnapshotName - timestamp to full zfs name');
# should be a no-op
my $snapshot3_orig=$snapshot3;
$snapshot3=$host->sanitiseSnapshotName($snapshot3);
is($snapshot3, $snapshot3_orig, 'sanitiseSnapshotName - check no-op situation');

$snapshot2=$host->sanitiseSnapshotName($snapshot2);
my $expected_list=[$snapshot1, $snapshot2, $snapshot3];
is_deeply(\@snapshot_list, $expected_list, 'getValidatedSnapshots');

# test destroyUntaggedSnapshots
$host->holdSnapshot($tag, $snapshot1);
$host->destroyUntaggedSnapshots;
$expected_list=[$snapshot1];
@snapshot_list=$host->getValidatedSnapshots;
is_deeply(\@snapshot_list, $expected_list, 'destroyUntaggedSnapshots - one tagged, two untagged');

$host->releaseSnapshot($tag, $snapshot1);
$host->destroyUntaggedSnapshots;
$expected_list=[];
@snapshot_list=$host->getValidatedSnapshots;
is_deeply(\@snapshot_list, $expected_list, 'destroyUntaggedSnapshots - none tagged');

# test getHeldSnapshotsByTag
$snapshot1=$host->takeSnapshot;
sleep 1;
$snapshot2=$host->takeSnapshot;
$host->holdSnapshot($tag, $snapshot1);
@snapshot_list=$host->getHeldSnapshotsByTag($tag);
isa_ok(\@snapshot_list, 'ARRAY');
$expected_list=[$snapshot1];
is_deeply(\@snapshot_list, $expected_list, 'getHeldSnapshotsByTag - one with tag, one without');

# test releaseAllWithTag
my @tags=('test1', 'test2');

$host->releaseAllWithTag($tag);
cmp_ok($host->countSnapshotsWithTag($tag), '==', 0, 'releaseAllWithTag');

# test releaseAllHoldsOnSnapshot
foreach $tag(@tags) {
	$host->holdSnapshot($tag, $snapshot1);
}
@holds=$host->getHoldsOnSnapshot($snapshot1);
@expected_holds=@tags;
is_deeply([sort @holds], [sort @expected_holds], 'getHoldsOnSnapshot - one snapshot, two tags');

$host->releaseAllHoldsOnSnapshot($snapshot1);
@expected_holds=();
@holds=$host->getHoldsOnSnapshot($snapshot1);
is_deeply(\@holds, \@expected_holds, 'releaseAllHoldsOnSnapshot - no holds left');

# test releaseAllHolds
foreach $tag(@tags) {
	$host->holdSnapshot($tag, $snapshot1);
	$host->holdSnapshot($tag, $snapshot2);
}
$host->releaseAllHolds;
@holds=$host->getHoldsOnSnapshot($snapshot1);
is_deeply(\@holds, \@expected_holds, 'releaseAllHolds - no holds left on snapshot1');
@holds=$host->getHoldsOnSnapshot($snapshot2);
is_deeply(\@holds, \@expected_holds, 'releaseAllHolds - no holds left on snapshot2');

$host->destroyZFSRecursively;
