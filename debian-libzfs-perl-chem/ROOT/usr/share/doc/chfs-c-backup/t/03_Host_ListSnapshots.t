#!/usr/bin/perl -I/usr/local/lib/perl5/site_perl/
$|=1;
use strict;
use warnings;

use ZFS::Host;

use Test::More tests => 15;
use Data::Dumper;

my $testhost='splot3.ch.private.cam.ac.uk';
my $zfsname="zpool1/test$$";

my $host=ZFS::Host->new();
$host->hostname($testhost);
print "Testing with zfs $zfsname on $testhost\n";

$host->zfsName($zfsname);
$host->createZFS($zfsname);
# take a snapshot now, and then a further three, each 2 seconds apart
my $snapshot1=$host->takeSnapshot;
sleep 1;
my $snapshot2=$host->takeSnapshot;
sleep 1;
my $snapshot3=$host->takeSnapshot;
sleep 1;
my $snapshot4=$host->takeSnapshot;

my $t1=$snapshot1;
$t1 =~ s/.*@//;
my $t2=$snapshot2;
$t2 =~ s/.*@//;
my $t3=$snapshot3;
$t3 =~ s/.*@//;
my $t4=$snapshot4;
$t4 =~ s/.*@//;


my $tstart=($t2-$t1)/2+$t1;
my $tend=($t4-$t3)/2+$t3;

# should have: $t1, $tstart, $t2, $t3, $tend, $t4

my @expected_result=();
my @result=();

@expected_result=($t1, $tstart, $t2, $t3, $tend, $t4);
@result=sort(@expected_result);
is_deeply(\@result, \@expected_result, 'test timestamps are well-ordered'); 

@expected_result=();
@result=$host->getSnapshotsBefore($t1);
is_deeply(\@result, \@expected_result, "no snapshots earlier than the first");

@expected_result=($snapshot1);
@result=$host->getSnapshotsBefore($tstart);
is_deeply(\@result, \@expected_result, "one snapshot before tstart");

@expected_result=($snapshot1);
@result=$host->getSnapshotsBefore($t2);
is_deeply(\@result, \@expected_result, "one snapshot before t2");

@expected_result=($snapshot1, $snapshot2);
@result=$host->getSnapshotsBefore($t3);
is_deeply(\@result, \@expected_result, "two snapshots before t3");

@expected_result=();
@result=$host->getSnapshotsAfter($t4);
is_deeply(\@result, \@expected_result, 'no snapshots later than the last');

@expected_result=($snapshot4);
@result=$host->getSnapshotsAfter($tend);
is_deeply(\@result, \@expected_result, "one snapshot before tend");

@expected_result=($snapshot4);
@result=$host->getSnapshotsAfter($t3);
is_deeply(\@result, \@expected_result, "one snapshot after t3");

@expected_result=($snapshot3, $snapshot4);
@result=$host->getSnapshotsAfter($t2);
is_deeply(\@result, \@expected_result, "two snapshots after t2");

# tstart=tend (integer, integer)
@expected_result=($snapshot1);
@result=$host->getSnapshotsBetween($t1, $t1);
is_deeply(\@result, \@expected_result, 'getSnapshotsBetween - one between t1 and t1');

# tstart=tend (float, float)
@expected_result=();
@result=$host->getSnapshotsBetween($tstart, $tstart);
is_deeply(\@result, \@expected_result, 'getSnapshotsBetween - none between tstart and tstart');

# t1<tstart (integer, float)
@expected_result=($snapshot1);
@result=$host->getSnapshotsBetween($t1, $tstart);
is_deeply(\@result, \@expected_result, "getSnapshotsBetween - one between t1 and tstart");

# tstart<tend (float, float)
@expected_result=($snapshot2, $snapshot3);
@result=$host->getSnapshotsBetween($tstart, $tend);
is_deeply(\@result, \@expected_result, "getSnapshotsBetween - two between tstart and tend");

# getSnapshotsBetween should work with arguments in either order
@expected_result=($snapshot2, $snapshot3);
@result=$host->getSnapshotsBetween($tend, $tstart);
is_deeply(\@result, \@expected_result, "getSnapshotsBetween - two between tend and tstart");

@expected_result=($snapshot1, $snapshot2, $snapshot3, $snapshot4);
@result=$host->getSnapshotsBetween($t1, $t4);
is_deeply(\@result, \@expected_result, 'getSnapshotsBetween - four between t1 and t4');


$host->destroyZFSRecursively($zfsname);
