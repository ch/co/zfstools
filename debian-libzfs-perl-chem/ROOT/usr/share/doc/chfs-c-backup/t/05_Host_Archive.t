#!/usr/bin/perl -I/usr/local/lib/perl5/site_perl/
$|=1;
use strict;
use warnings;
use POSIX;
use ZFS::Host;
use ZFS::Config;

use Test::More tests => 1;
use Data::Dumper;

# Config.pm expects to read a config file. So, we'll embed one
# here as a heredoc, write it out, and read it back in again..

# TODO: refactor Config.pm so that src and dst hosts are optional.
# For now, though, we'll set them even though we're not going to use them

my $configdata=<<'CONFIG';

$main::src="splot3.ch.private.cam.ac.uk";
$main::dest="splot3.ch.private.cam.ac.uk";

my $hour=3600;
my $day=24*$hour;
my $week=7*$day;
my $month=4*$week;

%main::archivepolicy=(
  'hourly'=>{age=>$day,count=>12},
  );

1;

CONFIG

my $testhost='splot3.ch.private.cam.ac.uk';
my $zfsname="zpool1/test$$";

my $host=ZFS::Host->new();
$host->hostname($testhost);
print "Testing with zfs $zfsname on $testhost\n";

$host->zfsName($zfsname);

my $conffile;
do {
	$conffile=tmpnam();
} until sysopen(CONFFILE, $conffile, O_RDWR|O_CREAT|O_EXCL, 0666);

print CONFFILE $configdata;
close(CONFFILE);

my $config=ZFS::Config->new($conffile);

$host->createZFS($zfsname);
# wait a moment to ensure ZFS has been created
sleep 1;
$host->archiveconfig($config);
my $snapshot=$host->takeSnapshot;
my $timestamp=$snapshot;
my $hour=3600;
$timestamp =~ s/.*@//;
foreach my $i (1..11) {
	$timestamp+=$hour;
	$host->takeSnapshotCalled($timestamp);
}
$host->updateArchiveOfType('hourly');
# we've created 1+11 snapshots with hour gaps, and archivepolicy tags them
# every 2 hours => expect 6 tagged
cmp_ok($host->countSnapshotsWithTag('hourly'), '==', 6, '6 snapshots with hourly tags');
$host->releaseAllHolds;
$host->destroyZFSRecursively($zfsname);
