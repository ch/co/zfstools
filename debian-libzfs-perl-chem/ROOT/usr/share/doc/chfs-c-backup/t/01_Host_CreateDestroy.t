#!/usr/bin/perl -I/usr/local/lib/perl5/site_perl/
$|=1;
use strict;
use warnings;

use ZFS::Host;

use Test::More tests => 14;

my $testhost='splot3.ch.private.cam.ac.uk';
my $zfsname="zpool1/test$$";

my $host=ZFS::Host->new();
$host->hostname($testhost);
print "Testing with zfs $zfsname on $testhost\n";

isa_ok($host, 'ZFS::Host', 'object instantiated');
is($host->hostname, $testhost, 'hostname set correctly');

$host->createZFS($zfsname);
cmp_ok($host->zfsExists($zfsname), '==', 1, 'test zfs exists');
cmp_ok($host->destroyZFS, '==', 2, 'refusing to destroy ZFS without zfsName property set');
$host->zfsName($zfsname);
is($host->zfsName, $zfsname, 'zfsname property correctly set');
cmp_ok($host->destroyZFS, '==', 1, 'zfs successfully destroyed');

$host->createZFS($zfsname);
my $snapshot=$host->takeSnapshot;
cmp_ok($host->zfsExists($snapshot), '==', 1, 'snapshot successfully taken');
cmp_ok($host->destroyZFS, '==', 3, 'not able to destroy zfs which has children');
$host->destroySnapshot($snapshot);
cmp_ok($host->zfsExists($snapshot), '==', 0, 'snapshot destroyed');
cmp_ok($host->destroyZFS, '==', 1, 'zfs successfully destroyed');

$host->createZFS($zfsname);
$host->takeSnapshotCalled($$);
cmp_ok($host->zfsExists($zfsname.'@'.$$), '==', 1, 'creation of explicitly named snapshot ok');
$host->destroySnapshot($$);
cmp_ok($host->zfsExists($zfsname.'@'.$$), '==', 0, 'destruction of explicitly named snapshot ok');
cmp_ok($host->destroyZFS, '==', 1, 'zfs successfully destroyed');

$host->createZFS($zfsname);
$host->takeSnapshot;
$host->destroyZFSRecursively;
cmp_ok($host->zfsExists($zfsname), '==', 0, 'recursive destroy of ZFS with snapshot succeeds');
