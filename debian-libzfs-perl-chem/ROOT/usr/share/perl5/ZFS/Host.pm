package ZFS::Host;

use strict;
use warnings;

use ZFS::Config;

use Net::OpenSSH;
use Data::Dumper;
use Date::Parse;

sub new {
	my $proto = shift;
	my $class = ref($proto) || $proto;
	my $self = {
		'hostname' => shift,
		'sshConn' => undef,
		'user' => 'root',
		'zfsName' => undef,
		'isLocal' => undef,
		'archiveconfig' => undef,
		'type' => undef, # src or dst
		'dryrun' => undef,
	};
	bless($self, $class);
	return $self;
}

sub hostname {
	my $self=shift;
	if(@_) { $self->{'hostname'} = shift; }
	return $self->{'hostname'};
}

sub getListForPrune {
        my $self=shift;
        my $listall=shift;
        my $list={};
        my @snaps=$self->getValidatedSnapshots($listall);
        s/^$self->{'zfsName'}\@// for(@snaps);
        foreach my $snap (@snaps) {
                $list->{$snap}=[$self->getHoldsOnSnapshot($snap)];
        }
        return $list;
}


sub isLocal {
	my $self=shift;
	if(! defined $self->{'isLocal'}) {
		my $fqdn=`hostname -f`;
		chomp $fqdn; 
		my $setname=$self->{'hostname'};
		my $h1=`getent hosts $fqdn`;
		my $h2=`getent hosts $setname`;
		chomp $h1;
		chomp $h2;
		if($h1 eq $h2) { 
			$self->{'isLocal'} = 1;
		} else {
			$self->{'isLocal'} = 0;
		}
	}
	return $self->{'isLocal'};
} 

sub zfsName {
	my $self=shift;
	if(@_) { $self->{'zfsName'}=shift; }
	return $self->{'zfsName'};
}

sub createZFS {
	my $self=shift;
	my $name;
	if(@_) {
		$name=shift;
	} elsif(defined($self->{'zfsName'})) {
		$name=$self->zfsName;
	} else {
		die "no name for ZFS";
	}
	my $cmd="/sbin/zfs create $name";
	$self->runCmd($cmd);
}

sub destroyZFSRecursively {
	my $self=shift;
	$self->destroyZFS("recurse");
}

sub destroyZFS {
	my $self=shift;
	my $recurse=shift;
	if(!defined($self->zfsName)) {
		warn('no zfsName set, so not destroying anything');
		return 2;
	}
	my $cmd;
	if($recurse) {
		$cmd="/sbin/zfs destroy -r ".$self->zfsName;
	} else {
		$cmd="/sbin/zfs destroy ".$self->zfsName;
	}
	$self->runCmd($cmd);
	if($self->zfsExists($self->zfsName)) {
		warn("destroyZFS failed; ".$self->zfsName." still exists");
		return 3;
	}
	return 1;
}

sub archiveconfig {
	my $self=shift;
	if(@_) { $self->{'archiveconfig'}=shift; }
	return $self->{'archiveconfig'};
}

sub type {
	my $self=shift;
	if(@_) { 
		my $type = shift;
		if($type eq "src" or $type eq "dst") {
			$self->{'type'} = $type; 
		} else {
			die "Error setting type of ".$self->hostname."; must be either src or dst, not $type\n";
		}
	}
	return $self->{'type'};
}

sub sshConn {
	my $self=shift;
	if(@_) { $self->{'sshConn'} = shift; }
	return $self->{'sshConn'};
}

sub user {
	my $self=shift;
	if(@_) { $self->{'user'} = shift; }
	return $self->{'user'};
}

sub connect {
	my $self = shift;
	my $conn=Net::OpenSSH->new($self->hostname, user => $self->user)
		or die "failed establishing connection to $self->hostname";
	$self->sshConn($conn);
}

sub dryrun {
	my $self=shift;
	if(@_) { $self->{'dryrun'} = shift; }
	return $self->{'dryrun'};
}

sub setDryrun {
	my $self=shift;
	$self->dryrun("yes");
}

sub runCmd {
	my $self = shift;
	my $cmd = shift;
	my $isreadonly = shift;
	if($self->dryrun) {
		print "$self->{'hostname'}: $cmd\n";
	}

	my $shouldrun=1;
	if($self->dryrun and ! defined($isreadonly)) {
		$shouldrun=0;
	}

	if($shouldrun) {
		if($self->isLocal) {
			return `$cmd`;
		} else {
			if(!$self->sshConn) {
				$self->connect;
			}
			my @out=$self->sshConn->capture($cmd);
			return @out;
		}
	}
}

sub runCmdLocally {
	my $self=shift;
	my $cmd=shift;
	return `$cmd`;
}

sub runCmdVerbose {
	my $self = shift;
	my $cmd = shift;
	print time.", ";
	print $self->hostname.": ";
	print $cmd."\n";
	$self->runCmd($cmd);
}


# NB - returns a list of *probable* snapshots that have snapshot names consisting only of digits;
# the output of `ls .zfs/snapshot` might include some ZFSs that have been (recently?) destroyed.
# You thus need to check via $self->zfsExists($name) that $name is indeed an extant ZFS before
# doing anything with it.
# array elements are e.g. data/group/ucc/alt36@123456789

sub getSnapshots {
	my $self=shift;
	my $listall=shift;

	if(! $self->zfsExists($self->{'zfsName'})) { return (); } 
	#$self->zfsIsMounted or $self->mount;
	$self->mount;
	my $cmd="ls -1 '".$self->getMountPoint."/.zfs/snapshot/'";

	my @snapnames=$self->runCmd($cmd, "isreadonly");
	if(!$listall) { @snapnames=grep(/^\d+$/, @snapnames); }
	chomp(@snapnames);

	return(map{qq#$self->{'zfsName'}\@$_#} @snapnames);

}

sub getValidatedSnapshots {
	my $self=shift;
	my $listall=shift;
	my @candidates=$self->getSnapshots($listall);
	my $allcandidates=join(" ", @candidates);
	# zfsexists will print a list of ZFSs that don't exist
	my $cmd="/usr/sbin/zfsexists $allcandidates";
	my @out=$self->runCmd($cmd, "isreadonly");
	if(! @out) { # no output => the entire list is good
		return sort(@candidates);
	} else { # some bad apples => remove them
		chomp(@out);
		my %toremove;
		@toremove{@out}=undef;
		return sort(grep{not exists $toremove{$_}} @candidates);
	}	
}

sub getArrayOfTagInfo {
	my $self=shift;
	my $listall=shift;
	my $list={};
	my @snaps=$self->getValidatedSnapshots($listall);
	s/^$self->{'zfsName'}\@// for(@snaps);
	foreach my $snap (@snaps) {
		$list->{$snap}=[$self->getHoldsOnSnapshot($snap)];
	}
	return $list;
}

sub printHoldsReport {
	my $self=shift;
	my $listall=shift;
	my $list=$self->getArrayOfTagInfo($listall);
	foreach my $snap(sort(keys(%{$list}))) {
		my $holds=join(" ", @{$list->{$snap}});
		print $self->zfsName."\@".$snap."\t".$holds."\n";
	}
}

sub getZFSCreationTime {
	my $self=shift;
	my $zfs=shift;
	if(! defined($zfs) ) { $zfs=$self->zfsName; }
	my $cmd="/sbin/zfs get -H -ovalue -p creation $zfs";
	my @out=$self->runCmd($cmd, "isreadonly");
	chomp @out;
	return $out[0];
}

sub getSnapshotsAfter {
	my $self=shift;
	my $time=shift;
	my $listall=shift;
	my @wantedsnaps=grep $self->getZFSCreationTime($_)>$time, $self->getSnapshots($listall);
	return @wantedsnaps;
}

sub getSnapshotsBefore {
	my $self=shift;
	my $time=shift;
	my $listall=shift;
	my @wantedsnaps=grep $self->getZFSCreationTime($_)<$time, $self->getSnapshots($listall);
	return @wantedsnaps;
}

sub getSnapshotsBetween {
	my $self=shift;
	my $start=shift;
	my $end=shift;
	my $listall=shift;
	if($end<$start) { 
		my $t=$start;
		$start=$end;
		$end=$t;
	}
	my @allsnaps=$self->getSnapshots($listall);
	my @wantedsnaps=grep {$self->getZFSCreationTime($_)>=$start and $self->getZFSCreationTime($_)<=$end} @allsnaps;
	return @wantedsnaps;
}

# returns either the ZFS which is the parent of $self->{'zfsName'}, 
# or $self->{'zfsName'} if it's already at the top of the heirarchy.
# N.B. We're assuming that the zpool is laid out as
#   zfs1/zfs2/zfs3/subdir1/subdir2
# and not as 
#   zfs1/zfs2/subdir1/zfs3

sub getParentZFS {
	my $self = shift;
	my $parent = $self->{'zfsName'};
	if ($self->{'zfsName'} =~ m#/#) {
		my @components = split(/\//, $parent);
		pop @components;
		$parent=join('/', @components);
	}
	return $parent;
}

# set the parent ZFS of self->{'zfsName'} to be writeable, but only 
# if we are a backup destination (i.e. don't change attribs of the source!)

sub setParentZFSwriteable {
	my $self = shift;
	print $self->hostname. " called setParentZFSwriteable\n";
	if($self->type eq "src") { 
		print $self->hostname." is a source; refusing to set readonly property\n";
	} elsif($self->type eq "dst") {
		my $cmd="/sbin/zfs set readonly=off ".$self->getParentZFS;
		$self->runCmd($cmd);
	}
}

# set the parent ZFS of self->{'zfsName'} to be readonly, but only 
# if we are a backup destination (i.e. don't change attribs of the source!)

sub setParentZFSreadonly {
	my $self = shift;
	if($self->type eq "src") { 
		print $self->hostname." is a source; refusing to set readonly property\n";
	} elsif($self->type eq "dst") {
		my $cmd="/sbin/zfs set readonly=on ".$self->getParentZFS;
		$self->runCmd($cmd);
	}
}

sub getChildFilesystems {
	my $self=shift;
	my $parent = shift;
	if(! defined($parent)) { $parent=$self->zfsName; }
	my $cmd="/sbin/zfs list -H -oname -r $parent";
	my @children=$self->runCmd($cmd, "isreadonly");
	chomp(@children);
	return(@children);
}


# returns eg data/scratch@1339680238
sub getLastSnapshotFullName {
	my $self=shift;
	my @snaps=$self->getSnapshots;
	if(@snaps) {
		return $snaps[-1];
	} 
}

# returns just bit after the "@" 
sub getLastSnapTime {
	my $self=shift;
	my $lastSnapName=$self->getLastSnapshotFullName;
	if(defined $lastSnapName) {
		$lastSnapName =~ s/.*@//;
		return $lastSnapName;
	}
}

# take a snapshot, and return its name.
# note that the snapshot name is a lower bound on when the snapshot
# was taken (zfs snapshot command may run later...). Thus, if the
# timing info is critical, use getZFSCreationTime rather than relying
# on the snapshot name
sub takeSnapshot {
	my $self=shift;
	my $recurse=shift;
	my $now=$self->getCurrentTime;
	my $snapname=$self->zfsName."@".$now;
	my $cmd;
	if($recurse) {
		$cmd="/sbin/zfs snapshot -r $snapname";
	} else {
		$cmd="/sbin/zfs snapshot $snapname";
	}
	$self->runCmd($cmd);
	return $snapname;
}

# returns current unix timestamp on the host
# NB this is important when running a command on remote host via ssh:
# their time might not be the same as ours..
sub getCurrentTime {
	my $self=shift;
	my $cmd="date +%s";
	my @out=$self->runCmd($cmd);
	chomp @out;
	return $out[0];
}

sub takeSnapshotCalled {
	my $self=shift;
	my $name=shift;
	my $recurse=shift;
	my $snapname=$self->zfsName."@".$name;
	my $cmd;
	if($recurse) {
		$cmd="/sbin/zfs snapshot -r $snapname";
	} else {
		$cmd="/sbin/zfs snapshot $snapname";
	}
	$self->runCmd($cmd);
}

# it suffices to make the parent read-only, assuming you haven't deliberately messed with inheritence
sub makeReadonly {
	my $self=shift;
	if($self->type eq "src") { 
		print $self->hostname." is a source; refusing to set readonly property\n";
	} elsif($self->type eq "dst") {
		my $cmd="/sbin/zfs set readonly=on $self->{'zfsName'}";
		$self->runCmd($cmd);
	}
}

sub mount {
	my $self=shift;
	if(! $self->zfsIsMounted ) {
		my $cmd="/sbin/zfs mount $self->{'zfsName'} >/dev/null 2>&1";
		# try to mount. the usual reason for failing is that the parent is readonly,
		# so if the command fails we try making the parent writeable. 
		$self->runCmd($cmd);
		if(!$self->zfsIsMounted) {
			print "mount failed, making parent writeable\n";
			if($self->type eq "dst") { $self->setParentZFSwriteable; }
			$self->runCmd($cmd);
			if($self->type eq "dst") { $self->setParentZFSreadonly; }
		}
	} 
	# sometimes see the case where the mounted property of a ZFS is yes, but
	# it isn't really mounted. If so, we umount and mount again
	if($self->zfsIsMounted and  (! $self->dotzfsDirExists)) {
		my $cmd="/sbin/zfs umount $self->{'zfsName'} >/dev/null 2>&1";
		$self->runCmd($cmd);
		$cmd="/sbin/zfs mount $self->{'zfsName'} >/dev/null 2>&1";
		$self->runCmd($cmd);
	}
}

sub zfsIsMounted {
	my $self=shift;
	my $cmd="/sbin/zfs get -H -ovalue mounted $self->{'zfsName'}";
	my @out=$self->runCmd($cmd,"isreadonly");
	if($out[0] =~ m/^yes$/) { return 1; } else { return 0; }
}

sub getMountPoint {
	my $self=shift;
	my $cmd="/sbin/zfs get -H -ovalue mountpoint $self->{'zfsName'}";
	my @out=$self->runCmd($cmd, "isreadonly");
	chomp(@out);
	return $out[0];
}

sub dotzfsDirExists {
	my $self=shift;
	my $cmd="[ -d \"".$self->getMountPoint."/.zfs\" ] && echo yes";
	my @out=$self->runCmd($cmd, "isreadonly");
	if(@out && $out[0] =~ m/^yes$/) { return 1; } else { return 0; }
}


sub zfsExists {
	my $self=shift;
	my $name=shift;
	my $cmd="/usr/sbin/zfsexists $name";
	my @out=$self->runCmd($cmd,"isreadonly");
	if(@out) { return 0; } else { return 1; }
}

sub holdSnapshot {
	my $self=shift;
	my $tag=shift;
	my $snapshot=shift;
	$snapshot=$self->sanitiseSnapshotName($snapshot);
	my $cmd="/sbin/zfs hold $tag $snapshot";
	$self->runCmd($cmd) unless $self->snapshotHasTag($tag,$snapshot);
}

sub releaseSnapshot {
	my $self=shift;
	my $tag=shift;
	my $snapshot=shift;
	$snapshot=$self->sanitiseSnapshotName($snapshot);
	my $cmd="/sbin/zfs release $tag $snapshot";
	$self->runCmd($cmd) if $self->snapshotHasTag($tag,$snapshot);
}

sub destroySnapshot {
	my $self=shift;
	my $snapshot=$self->sanitiseSnapshotName(shift);
	if (! $snapshot =~ m/.*\@.*/) {
		print "$snapshot is not a snapshot, so refusing to destroy\n";
		return ;
	}
	my $cmd="/sbin/zfs destroy $snapshot";
	$self->runCmd($cmd);
}

sub rollback {
	my $self=shift;
	my $snapshot=shift;
	$snapshot=$self->sanitiseSnapshotName($snapshot);
	my $cmd="/sbin/zfs rollback -r $snapshot";
	$self->runCmd($cmd);
}

sub sanitiseSnapshotName {
	my $self=shift;
	my $snapshot=shift;
	if ($snapshot !~ /.*\@.*/) {
		$snapshot=$self->zfsName."@".$snapshot;
	} else {
		my $zfs=(split(/\@/, $snapshot))[0];
		if($zfs ne $self->zfsName) {
			die "Trying to act on $zfs but I'm set up for $self->{'zfsName'}\n";
		}
	}

	return $snapshot;
}

sub snapshotHasTag {
	my $self=shift;
	my $searchtag=shift;
	my $snapshot=$self->sanitiseSnapshotName(shift);
	my $cmd="/sbin/zfs holds -H $snapshot";
	my @out=$self->runCmd($cmd,"isreadonly");
	foreach my $line(@out) {
		my $tag=(split(/\s+/,$line))[1];
		if($tag eq $searchtag) { return 1; }
	}
	# if we get here, we don't have the tag
	return 0;
}

sub getHeldSnapshotsByTag {
	my $self = shift;
	my $searchtag = shift;
	my $listall = shift;
	my @heldSnapshots=();
	my @snapshots=$self->getSnapshots($listall);
	foreach my $snapshot(@snapshots) {
		if($self->snapshotHasTag($searchtag,$snapshot)) { 
			push(@heldSnapshots, $snapshot);
		}
	}
	return @heldSnapshots;
}

sub getHoldsOnSnapshot {
	my $self=shift;
	my $snapshot=$self->sanitiseSnapshotName(shift);
	my $cmd="/sbin/zfs holds -H $snapshot";
	my @out=$self->runCmd($cmd,"isreadonly");
	@out=map{(split(/\s+/,$_))[1]} @out;
	return @out;
}

sub releaseAllWithTag {
	my $self=shift;
	my $tag=shift;
	my @held=$self->getHeldSnapshotsByTag($tag);
	foreach my $snapshot(@held) {
		$self->releaseSnapshot($tag,$snapshot);
	}
}

sub releaseAllHoldsOnSnapshot {
	my $self=shift;
	my $snapshot=shift;
	my @holds=$self->getHoldsOnSnapshot($snapshot);
	foreach my $hold(@holds) {
		$self->releaseSnapshot($hold, $snapshot);
	}
}

sub releaseAllHolds {
	my $self=shift;
	my @snapshots=$self->getValidatedSnapshots;
	foreach my $snapshot(@snapshots) {
		$self->releaseAllHoldsOnSnapshot($snapshot);
	}
}

sub addArchiveTags {
	my $self=shift;
	foreach my $type($self->getArchiveTypes) {
		print "archive type: $type\n";
	}
}

sub updateArchiveOfType {
	my $self=shift;
	my $type=shift;

	my $maxage=$self->getLastSnapTime-$self->{'archiveconfig'}->ageOfArchiveType($type);
	my $step=$self->{'archiveconfig'}->ageOfArchiveType($type)/$self->{'archiveconfig'}->numberOfArchiveType($type);
	my $c=$self->countSnapshotsWithTag($type);
	# bootstrap, if we have no snapshots with this archive tag
	if($c==0) {
		my @candidates=$self->getValidatedSnapshots;
		my $firstcandidate=$candidates[0];
		$self->holdSnapshot($type, $firstcandidate);
	}
	my $newestOfType=$self->newestSnapshotWithTag($type);
	$newestOfType =~ s/.*@//;
	my $tWant=$newestOfType+$step;
	while(my $nextToTag=$self->firstSnapshotNewerThan($tWant)) {
		$self->holdSnapshot($type, $nextToTag);
		$tWant=$nextToTag+$step;
	}
	#TODO: remove hold tag from snapshots older than $maxage
}

sub countSnapshotsWithTag {
	my $self=shift;
	my $tag=shift;
	my $taglist=$self->getArrayOfTagInfo;
	my $count=scalar(grep { grep /$tag/,@{$taglist->{$_}};} keys(%{$taglist}));
	return $count;
}

sub newestSnapshotWithTag {
	my $self=shift;
	my $tag=shift;
	my $taglist=$self->getArrayOfTagInfo;
	my @list=reverse(sort(grep { grep /$tag/,@{$taglist->{$_}};} keys(%{$taglist})));
	return $list[0];
}

# strictly, this ought to be firstSnapshotAtLeastAsNewAs but that's awkward..
sub firstSnapshotNewerThan {
	my $self=shift;
	my $age=shift;
	my @list=(sort(grep {$_>=$age} keys(%{$self->getArrayOfTagInfo})));
	if(scalar(@list)>0) {
		return $list[0];
	} else {
		return 0;
	}
}

sub getArchiveTypes {
	my $self=shift;
	if(defined($self->{'archiveconfig'})) {
		return $self->archiveconfig->getArchiveTypes;
	} else {
		die "no archivepolicy set";
	}
}

sub destroyUntaggedSnapshots {
	my $self=shift;
	my $taglist=$self->getArrayOfTagInfo;
	foreach my $snapshot (keys(%{$taglist})) {
		if (scalar(@{$taglist->{$snapshot}})==0) {
			delete $taglist->{$snapshot};
			$self->destroySnapshot($snapshot);
		}
	}
}

1;
