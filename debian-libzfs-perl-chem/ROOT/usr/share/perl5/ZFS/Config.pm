package ZFS::Config;

use strict;
use warnings;

use Data::Dumper;

sub new {
	my $proto = shift;
	my $class = ref($proto) || $proto;
	my $self = {
		'filename' => shift,
		'src' => undef,
		'dest' => undef,
		'zfsNames' => undef,
		'ignoreList' => undef,
		'archivepolicy' => undef,
	};

	bless($self, $class);
	if ($self->filename) {
		$self->parseFile;
	}
	$self->verifyConfig;
	return $self;

}

sub void {
        my $proto = shift;
        my $class = ref($proto) || $proto;
        my $self = {
                'filename' => shift,
                'src' => undef,
                'dest' => undef,
                'zfsNames' => undef,
                'ignoreList' => undef,
                'archivepolicy' => undef,
        };

        bless($self, $class);
        return $self;
}


sub filename {
	my $self = shift;
	return $self->{'filename'};
}

sub zfsNames {
	my $self = shift;
	if (@_) { $self->{'zfsNames'} = shift; }
	return $self->{'zfsNames'};
}

sub archivepolicy {
	my $self = shift;
	if(@_) { $self->{'archivepolicy'} = shift; }
	return $self->{'archivepolicy'};
}

sub src {
	my $self = shift;
	if (@_) { $self->{'src'} = shift; }
	return $self->{'src'};
}

sub dest {
	my $self = shift;
	if (@_) { $self->{'dest'} = shift; }
	return $self->{'dest'};
}

sub parseFile {
	my $self = shift;
	require $self->filename;
	$self->zfsNames(\%main::zfsNames);
	$self->src($main::src);
	$self->dest($main::dest);
	$self->archivepolicy(\%main::archivepolicy);
	undef(%main::src);
	undef(%main::dest);
	$self->zfsNames || die "Failed to import ".$self->filename." (bad zfsNames)\n";
	$self->src || die "Failed to import ".$self->filename." (bad src)\n";
	$self->dest || die "Failed to import ".$self->filename." (bad dest)\n";

}

sub dump {
	my $self = shift;
	print Dumper($self);
}

sub getArchiveTypes {
	my $self=shift;
	return keys(%{$self->archivepolicy});
}

sub ageOfArchiveType {
	my $self=shift;
	my $type=shift;
	return $self->archivepolicy->{$type}->{'age'};
}

sub numberOfArchiveType {
	my $self=shift;
	my $type=shift;
	return $self->archivepolicy->{$type}->{'count'};
}

sub verifyConfig {
	my $self = shift;
	foreach my $zfs (keys %{$self->zfsNames}) {
		my $re1 = '\w+';
		my $re2 = '(\w+/)*';
		my $re3 = '(\w+)?';
		my $re4 = '[^/]$';
		my $regex = $re1.$re2.$re3.$re4;
		$self->validateZFSname($zfs) or die "$zfs is not a well-formed ZFS name";
		foreach my $child (@{$self->zfsNames->{$zfs}}) {
			$self->validateZFSname($child) or die "$child is not a well-formed ZFS name";
			$self->checkZFSisChildOf($child, $zfs) or die "$child is not a child of $zfs";
			$self->ignore($child);
		}
	}
	$self->validateArchivePolicy;
}

sub validateZFSname {
	my $self = shift;
	my $name = shift;
	my $re1 = '\w+';
	my $re2 = '(\w+/)*';
	my $re3 = '(\w+)?';
	my $re4 = '[^/]$';
	my $regex = $re1.$re2.$re3.$re4;
	$name =~ m#^$regex#;
}

sub validateArchivePolicy {
	my $self=shift;
	foreach my $type ($self->getArchiveTypes) {
		defined($self->ageOfArchiveType($type)) or die "archivetype $type needs an age";
		defined($self->numberOfArchiveType($type)) or die "archivetype $type needs a count";
	}
}

sub checkZFSisChildOf {
	my $self = shift;
	my $child = shift;
	my $parent = shift;
	if($child =~ m#^$parent/#) { return 1 } else { return 0 };
}

sub ignore {
	my $self = shift;
	my $item = shift;
	defined $self->{'ignoreList'} or $self->{'ignoreList'} = [];
	push(@{$self->{'ignoreList'}}, $item);
}

sub ignoreList {
	my $self = shift;
	return $self->{'ignoreList'};
}

1;

