#include <libzfs.h>

int main(int argc, char** argv) {

	int ret=0;
	int i=0;
	if(argc==1) {
		fprintf(stdout, "Usage: %s [zfsname] [zfsname] ....\n"
				"If any of the zfsnames do not exist, they will be printed"
				"back to stdout\n", argv[0]);
	}


	libzfs_handle_t *g_zfs;

	if ((g_zfs = libzfs_init()) == NULL) {
		(void) fprintf(stderr, "internal error: failed to "
				"initialize ZFS library\n");
		return (1);
	}
	for(i=1; i<argc; i++) {
		if (! zfs_dataset_exists(g_zfs, argv[i],ZFS_TYPE_DATASET)) {
			fprintf(stdout, "%s\n",argv[i]);
			ret=1;
		}
	}
	return ret;
}
