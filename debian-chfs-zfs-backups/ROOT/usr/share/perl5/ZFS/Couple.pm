# To represent a ZFS object of type Filesystem
package ZFS::Couple;
use ZFS::Filesystem;
use Array::Utils qw(:all);

use strict;
use warnings;

sub new() {
 my $class=shift;
 my $self=shift || {};
 bless $self, $class;
 return $self;
}
sub setleft() {
 my $self=shift;
 $self->{'Z_left'}=shift;
}
sub setright() {
 my $self=shift;
 $self->{'Z_right'}=shift;
}

sub populate() {
 my $self=shift;
 my $recurse=shift;
 $self->{'Z_right'}->populate($recurse);
 $self->{'Z_left'}->populate($recurse);
}

sub latestcommonbackup() {
 my $self=shift;
 # Find highest-numbered snapshot common to both left and right
 my @lsnaps=keys(%{$self->{'Z_left'}->{'_snapshots'}});
 my @rsnaps=keys(%{$self->{'Z_right'}->{'_snapshots'}});
 my @intersect=intersect(@lsnaps,@rsnaps);
 my @l=sort {my $aa=$a; my $bb=$b; $aa=~ s/.*@//; $bb =~ s/.*@//; $aa <=> $bb }  @intersect;
 return $l[0] if $l[0];
}

sub setlatestcommonbackup() {
 my $self=shift;
 my $snapname=shift;
 $self->{'Z_left'}->setlatestcommonbackup($snapname);
 $self->{'Z_right'}->setlatestcommonbackup($snapname);
}

1;
