# To represent a ZFS object of type Filesystem
package ZFS::Dataset;
use ZFS::Snapshot;
use Net::OpenSSH;
use Array::Utils qw(:all);

use strict;
use warnings;

sub new() {
 my $class=shift;
 my $self=shift || {};
 bless $self, $class;
 $self->{'_children'}={};
 $self->connectRemote() if $self->{'host'};
 $self->{'_holdtypes'}={};
 $self->{'_timecmd'}=0;
 return $self;
}

sub connectRemote() {
 my $self=shift;
 $self->{'_sshconn'}=Net::OpenSSH->new($self->{'host'}) || die "Failed to connect to $self->{'host'} via SSH: $!";
}

sub _getchildren() {
 my $self=shift;
 my $type=shift;
 return unless $self->{'name'};
 my $cmd="zfs list -H -o name -d 1 -t $type $self->{'name'}";
 my $list=$self->_runCmd($cmd);
 my @c=grep {!/^$self->{'name'}$/} @{$list};
 return \@c;
}

sub _runCmd() {
 my $self=shift;
 my $cmd=shift;
 my @out;
 if ($self->{'host'}) {
  $self->connectRemote() unless ($self->{'_sshconn'});
  my $s=time;
  @out=$self->{'_sshconn'}->capture($cmd);
  print "Took ".(time-$s)."s to execute ".$cmd." on ".$self->{'host'}."\n" if $self->{'_timecmd'};
 } else {
  my $s=time;
  @out=`$cmd`;
  print "Took ".(time-$s)."s to execute ".$cmd."\n" if $self->{'_timecmd'};
 }
 chomp(@out);
 return \@out;
}

sub _runZfsCmdInChunks() {
  my $self = shift;
  my $zfscmd = shift;
  my $argsref = shift;
  my @out;
  # @args will be consumed by splice() so we are deliberately taking a copy here
  my @args = @$argsref;

  # I believe we get away with 2015 arguments to exec(), which ultimately is what
  # gets called by OpenSSH.pm . But the exec() has a few other arguments, e.g.
  # 'echo zfs release $hold' so we'll round down to give ourselves some headroom
  my $chunksize = 2000;
  while(my @chunk = splice(@args, 0, $chunksize)) {
    my $cmd="$zfscmd ".join(' ',@chunk);
    push(@out, @{$self->_runCmd($cmd)});
  }
  return \@out;
}

1;
