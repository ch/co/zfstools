# To represent a list of tasks to do on a ZFS
package ZFS::Tasklist;
use ZFS::Task;

use strict;
use warnings;

sub new() {
 my $class=shift;
 my $self=shift || {};
 $self->{'_tasks'}=[] unless $self->{'_tasks'};
 bless $self, $class;
 return $self;
}

sub prepend() {
 my $self=shift;
 my $task=shift;
 unshift(@{$self->{'_tasks'}},$task);
}

sub addtask() {
 my $self=shift;
 my $action=shift; 
 my $tag=shift;
 my $parent=shift;
 if ($action eq 'release') {
  # Look for matching action of 'hold' and remove it if it exists
  if ($self->hastask('hold',$tag)) {
   $self->deltask('hold',$tag);
   $parent->deltag($tag);
  } else {
   push(@{$self->{'_tasks'}},ZFS::Task->new({action=>$action,arg=>$tag}));
   $parent->deltag($tag);
  }
 } elsif ($action eq 'hold') {
  # Look for matching action of 'release' and remove it if it exists
  if ($self->hastask('release',$tag)) {
   $self->deltask('release',$tag);
   $parent->addtag($tag);
  } else {
   push(@{$self->{'_tasks'}},ZFS::Task->new({action=>$action,arg=>$tag}));
   $parent->addtag($tag);
  }
 } elsif ($action eq 'destroy') {
  push(@{$self->{'_tasks'}},ZFS::Task->new({action=>$action}));
  $parent->destroy();
 } else {
  die "Unknown action $action\n";
 }
}

sub deltask() {
 my $self=shift;
 my $action=shift;
 my $tag=shift;
 my @tasks= grep {! $_->matches($action,$tag)} @{$self->{'_tasks'}};
 $self->{'_tasks'}=\@tasks;
}

sub append() {
 my $self=shift;
 my $task=shift;
 push(@{$self->{'_tasks'}},$task);
}

sub joblist() {
 my $self=shift;
 my $parent=shift;
 return map { $_->job($parent)} @{$self->{'_tasks'}};
}

sub hastask() {
 my $self=shift;
 my $task=shift;
 my $tag=shift;
 # True if one of our tasks has a task to $task $tag
 my $result=0;
 map {$result=$result || $_->matches($task,$tag)} @{$self->{'_tasks'}};
 return $result;
}
1;
