# To represent a ZFS object of type Filesystem
package ZFS::Filesystem;
use base ZFS::Dataset;
use List::MoreUtils qw(uniq);
use ZFS::Snapshot;
use Array::Utils qw(:all);
#use DateTime::Format::Strptime;
use Time::Piece;
use Time::Duration;

use strict;
use warnings;

sub populate() {
 # get information about children
 my $self=shift;
 my $recurse=shift || 0;
 return unless $self->{'name'};
 # Load my creation time
 $self->_getcreation();
 # Load my snapshots
 #print "Populating snaps for $self->{'name'}\n";
 $self->populatesnaps();
 #print "Getting children for $self->{'name'}\n";
 my $children=$self->_getchildren('filesystem');
 #print "Adding children for $self->{'name'}\n";
 map {$self->_addchild($_)} @{$children};
 map {$self->{'_children'}->{$_}->populate($recurse);} keys(%{$self->{'_children'}}) if ($recurse==1);
}

sub _addchild() {
 my $self=shift; 
 my $name=shift;
 $self->{'_children'}->{$name}=new ZFS::Filesystem({'name'=>$name});
 $self->{'_children'}->{$name}->{'host'}=$self->{'host'} if $self->{'host'};
 $self->{'_children'}->{$name}->{'singledelete'}=$self->{'singledelete'} if $self->{'singledelete'};
}

sub populatesnaps() {
 my $self=shift;
 #my $snaps=$self->_getchildren('snapshot');
 #map {$self->{'_snapshots'}->{$_}=new ZFS::Snapshot({'name'=>$_}) } @{$snaps};
 my $s=time;
 #print "Started to populate at $s\n";
 map {$self->{'_snapshots'}->{$_}=new ZFS::Snapshot({'name'=>$_,'host'=>$self->{'host'}})} $self->_getsnapshots();
 #print "Ended population at ".time."\n";
 #print "Time to enumerate snapshots of $self->{'name'} ".($self->{'host'}." "?'on '.$self->{'host'}:'')."was ".(time-$s)." seconds\n";
 my $cmd="zfs holds -H ".join(' ',keys(%{$self->{'_snapshots'}}));
 $s=time;
 my $res=$self->_runCmd($cmd);
 #print "Time to enumerate holds ".($self->{'host'}?'on '.$self->{'host'}." ":'')."was ".(time-$s)." seconds\n";
 map {my @a=split(/\s+/,$_); $self->{'_snapshots'}->{$a[0]}->addHold($a[1])} @{$res};
}

sub _getsnapshots() {
 my $self=shift;
 my $cmd='zfs get -H -o value mountpoint '.$self->{'name'};
 my $res=$self->_runCmd($cmd);
 my $mnt=$res->[0];
 return unless $mnt;
 $cmd='ls -1 '.$mnt.'/.zfs/snapshot || true';
 $res=$self->_runCmd($cmd);
 my @snaps=map {s/^/$self->{'name'}@/;$_} @{$res};
 $cmd='/usr/sbin/zfsexists '.join(' ',@snaps)." 2>&1 || true";
 $res=$self->_runCmd($cmd);
 my @missing=@{$res};
 #print "Got ".join(',',@missing)." missing zfses\n";
 return array_minus(@snaps,@missing);
}

sub _getcreation() {
 my $self=shift;
 my $cmd='zfs get -H -o value creation '.$self->{'name'};
 my $res=$self->_runCmd($cmd);
 my $creation=$res->[0];
 chomp($creation);
 return unless $creation;

 my $tp=Time::Piece->strptime($creation, '%a %b %d %H:%M %Y');
 $self->{'created'}=$tp->epoch;
}

sub holdsreport() {
 my $self=shift;
 my @holds=uniq map { keys(%{$self->{'_snapshots'}->{$_}->{'_holds'}})} $self->snaplist();
 my @results=();
 for my $hold (@holds) {
  push(@results,map { $hold.':'.$_ } grep {$self->{'_snapshots'}->{$_}->{'_holds'}->{$hold} } $self->snaplist());
 }
 return \@results;
}

sub setcommonbackup() {
 my $self=shift;
 my $twin=shift;
 my $recurse=shift;

 die ('ZFS names are different') unless $self->{'name'} eq $twin->{'name'};
 my @a=$self->snaplist();
 my @b=$twin->snaplist();

 my @l=sort {my $aa=$a; my $bb=$b; $aa=~ s/.*@//; $bb =~ s/.*@//; $bb <=> $aa } intersect(@a,@b);
 if ($l[0]) {
  $self->{'_holdtypes'}->{'commonbackup'}=1;
  $twin->{'_holdtypes'}->{'commonbackup'}=1;

  $self->{'_snapshots'}->{$l[0]}->addtask('hold','commonbackup') unless $self->{'_snapshots'}->{$l[0]}->hashold('commonbackup');
  $twin->{'_snapshots'}->{$l[0]}->addtask('hold','commonbackup') unless $twin->{'_snapshots'}->{$l[0]}->hashold('commonbackup');

  map {$self->{'_snapshots'}->{$_}->addtask('release','commonbackup')} grep {($_ ne $l[0]) && $self->{'_snapshots'}->{$_}->hashold('commonbackup')} $self->snaplist();
  map {$twin->{'_snapshots'}->{$_}->addtask('release','commonbackup')} grep {($_ ne $l[0]) && $twin->{'_snapshots'}->{$_}->hashold('commonbackup')} $twin->snaplist();
 } else {
  print "No common backup tags found\n";
 }
 map {$self->{'_children'}->{$_}->setcommonbackup($twin->{'_children'}->{$_},$recurse)} grep {exists($twin->{'_children'}->{$_})} $self->childlist() if $recurse;
}

sub _latest() {
 my $self=shift;
 my @a=sort {my $aa=$a; my $bb=$b; $aa=~ s/.*@//; $bb =~ s/.*@//; $aa <=> $bb } $self->snaplist();
 return $a[-1] if @a;
}

sub _countbytag() {
 my $self=shift;
 my $tag=shift;
 return scalar($self->_grepbytag($tag));
}

sub _grepbytag() {
 my $self=shift;
 my $tag=shift;
 return grep {$self->{'_snapshots'}->{$_}->hashold($tag) } $self->snaplist();
}

sub _getmorerecent() {
 my $self=shift;
 my $stamp=shift;
 my $count=shift || undef;
 my @snaps=grep { my $a=$_;$a=~s/.*@//; $a > $stamp } $self->snaplist();
 if (defined($count)) {
  return @snaps[0 .. $count-1];
 } else {
  return @snaps;
 }
}

sub _getlessrecent() {
 my $self=shift;
 my $stamp=shift;
 my $count=shift || undef;
 my @snaps=grep { my $a=$_;$a=~s/.*@//; $a < $stamp } $self->snaplist();
 if (defined($count)) {
  return @snaps[0 .. $count-1];
 } else {
  return @snaps;
 }
}

sub prune() {
 my $self=shift;
 my $args=shift;
 my $recurse=shift;
 my $tag=$args->{'type'};
 my $age=$args->{'age'};
 my $count=$args->{'count'};
 my $latest=$self->_latest();
 $latest =~ s/.*@//;
 my $maxage=$latest-$age;
 my $step=$age/$count;
 # Count tags
 my $num = $self->_countbytag($tag);
 if ($num == 0) {
  #Bootstrap: Find the first snapshot with stamp greater than $maxage and assign the tag to that
  print "Bootstrapping for $tag\n";
  my $later=$self->_getmorerecent($maxage,1);
  if ($later && $self->{'_snapshots'}->{$later}) {
   $self->{'_snapshots'}->{$later}->addtask('hold',$tag);
  }
 }
 # Find most recent snapshot of the type
 my $latesttagged=($self->_grepbytag($tag))[0];
 $latesttagged =~ s/.*@//;
 my $nextstamp=$latesttagged+$step;
 # No good looking for snapshots newer than the latest snapshot
 while ($nextstamp <= $latest) {
  my $newersnap = $self->_getmorerecent($nextstamp,1);
  if ($newersnap) {
   $self->{'_snapshots'}->{$newersnap}->addtask('hold',$tag) unless $self->{'_snapshots'}->{$newersnap}->hashold($tag);
   $nextstamp=$self->{'_snapshots'}->{$newersnap}->astimestamp()+$step;
  } else {
   # Elephant in Cairo
   $nextstamp=$latest+1;
  }
 }
 
 # We seem to have too many weekly tags on a ZFS. Why?
 map {$self->{'_snapshots'}->{$_}->addtask('release',$tag)} grep {$self->{'_snapshots'}->{$_}->hashold($tag)} $self->_getlessrecent($maxage);
 my @prune=sort {my $aa=$a; my $bb=$b; $aa=~ s/.*@//; $bb =~ s/.*@//; $aa <=> $bb } grep {$self->{'_snapshots'}->{$_}->hashold($tag)} $self->snaplist();
 map { pop @prune } (1 .. $count);
 map {$self->{'_snapshots'}->{$_}->addtask('release',$tag)} @prune;
 $self->{'_holdtypes'}->{$tag}=1;
 map {$self->{'_children'}->{$_}->prune($args,$recurse);} $self->childlist() if $recurse;
}

sub joblist() {
 my $self=shift;
 my $recurse=shift;
 my @result= map { $self->{'_snapshots'}->{$_}->joblist()} $self->snaplist();
 map { push(@result,$self->{'_children'}->{$_}->joblist($recurse)) } $self->childlist() if $recurse;
 return @result;
}

sub deluntagged() {
 my $self=shift;
 my $recurse=shift;
 #map { $self->{'_snapshots'}->{$_}->appendtask(ZFS::Task->new({action=>'destroy'})) } grep { scalar(keys(%{$self->{'_snapshots'}->{$_}->{'_newholds'}}))==0 } $self->snaplist();
 map { $self->{'_snapshots'}->{$_}->addtask('destroy') } grep { scalar(keys(%{$self->{'_snapshots'}->{$_}->{'_newholds'}}))==0 } $self->snaplist();
 map { $self->{'_children'}->{$_}->deluntagged($recurse) } $self->childlist() if $recurse;
}

sub holdsummary() {
 my $self=shift;
 my $recurse=shift;
 my @result=map {$_.': '.join(',',keys(%{$self->{'_snapshots'}->{$_}->{'_holds'}}))} $self->snaplist();
 map { push(@result,$self->{'_children'}->{$_}->holdsummary($recurse)) } $self->childlist() if $recurse;
 return @result;
}
sub newholdsummary() {
 my $self=shift;
 my $recurse=shift;
 my @result=map {$_.': '.join(',',keys(%{$self->{'_snapshots'}->{$_}->{'_newholds'}}))} $self->snaplist();
#grep {scalar(keys(%{$self->{'_snapshots'}->{$_}->{'_newholds'}}))>0} $self->snaplist();
 map { push(@result,$self->{'_children'}->{$_}->newholdsummary($recurse)) } $self->childlist() if $recurse;
 return grep {defined($_)} @result;
}

sub actionsummary() {
 my $self=shift;
 my $recurse=shift;
 my $hidenoop=shift;
 my @result=map {$self->{'_snapshots'}->{$_}->actionsummary($hidenoop)} $self->snaplist();
 map { push(@result,$self->{'_children'}->{$_}->actionsummary($recurse,$hidenoop))} $self->childlist() if $recurse;
 return @result;
}

sub snaplist() {
 my $self=shift;
 return sort {my $aa=$a; my $bb=$b; $aa=~ s/.*@//; $bb =~ s/.*@//; $aa <=> $bb } keys(%{$self->{'_snapshots'}});
}

sub childlist() {
 my $self=shift;
 return keys(%{$self->{'_children'}});
}

sub holdtypes() {
 my $self=shift;
 my $recurse=shift;
 my @list=keys(%{$self->{'_holdtypes'}});
 map { push(@list,$self->{'_children'}->{$_}->holdtypes()) } $self->childlist() if $recurse;
 return @list;
}

sub snapstotag() {
 my $self=shift;
 my $recurse=shift;
 my $task=shift;
 my $tag=shift;
 my @result= grep { $self->{'_snapshots'}->{$_}->hastask($task,$tag)} $self->snaplist();
 map { push(@result,$self->{'_children'}->{$_}->snapstotag($recurse,$task,$tag)) } $self->childlist() if $recurse;
 #print "Found ".join(',',@result)." wanting $tag $task\n";
 return @result;
}

sub deletions() {
 my $self=shift;
 my $recurse=shift;
 # Return an array of commands to delete unwanted snapshots
 my @results=();
 # Look for snapshots to destroy
 my @del=grep { $self->{'_snapshots'}->{$_}->isdestroyed() } $self->snaplist();
 if ($self->{'singledelete'}) {
  map {push(@results,@{$self->_runCmd(($self->{'readonly'}?'echo ':'').'zfs destroy -d '.$_)})} @del
 } else {   
  if (@del) {
   my $cmd='zfs destroy -d '.shift(@del);
   $cmd.=','.join(',',map { s/.*@//;$_ } @del) if @del;
   push(@results,@{$self->_runCmd(($self->{'readonly'}?'echo ':'').$cmd)});
  } 
 }
 map {push(@results,$self->{'_children'}->{$_}->deletions($recurse))} $self->childlist() if $recurse;
 return @results;
} 

sub commit() {
 my $self=shift;
 my $recurse=shift;
 #$self->{'readonly'}=1;
 my @results=();
 # We can hold or release tags across any snapshots
 my @holdlist=unique($self->holdtypes($recurse));
 # Loop over these types of holds looking for snapshots to tag and untag
 for my $hold (@holdlist) {
  my @addsnaps=$self->snapstotag($recurse,'hold',$hold);
  if (@addsnaps) {
   my $zfscmd=($self->{'readonly'}?'echo ':'')."zfs hold $hold";
   push(@results, @{$self->_runZfsCmdInChunks($zfscmd, \@addsnaps)});
  }
  @addsnaps=$self->snapstotag($recurse,'release',$hold);
  if (@addsnaps) {
    my $zfscmd=($self->{'readonly'}?'echo ':'') . "zfs release $hold";
    push(@results, @{$self->_runZfsCmdInChunks($zfscmd, \@addsnaps)});
  }
 }
 # But we can only delete snapshots from ourself
 my @deletions=$self->deletions($recurse);
 push(@results,@deletions);
 return \@results;
}

sub countsnaps(&@) {
 my $self=shift;
 my $grep;
 if (defined($_[0])) {
  $grep=\&{shift @_};
 } else {
  shift;
 }
 my @results;
 if (defined($grep)) {
  @results = grep {$grep->($_)} $self->snaplist();
 } else {
  @results = $self->snaplist();
 } 
 return scalar(@results);
}

sub xymon() {
  my $self=shift;
  my $policy=shift;
  my $bb=shift;
  my $recurse=shift;
  my $commonage=48*3600;
  my $synctime=48*3600;
  my $unheldmax=30;
  my $sortkeys={hourly=>1,daily=>2,weekly=>3,monthly=>4};
  for my $type (sort { $sortkeys->{$a} <=> $sortkeys->{$b} }keys(%{$policy})) {
   # All snapshots get 36 hours grace 'cos they might not have been sent yet
   my $age=$policy->{$type}->{'age'}+($synctime);
   # ecount - number of snapshots according to policy (allowed +/- 50%)
   # ycount - number of snapshots according to age (lower limit)
   my $ecount=$policy->{$type}->{'count'};
   my $ycount=$ecount;
   # Now, how many snapshots are realistic?
   my $zfsage=time()-$self->{'created'};
   if ($zfsage<($policy->{$type}->{'age'})) {
    $ycount=int (1+$policy->{$type}->{'count'}*$zfsage/$policy->{$type}->{'age'});
   }
   my @snaps=grep { scalar(grep(/$type/, keys(%{$self->{'_snapshots'}->{$_}->{'_holds'}})))>0 } $self->snaplist();
   my $acount=scalar(@snaps);
   my $first=$snaps[0];
   $first =~ s/.*@//;
   my $last=$snaps[-1];
   $last =~ s/.*@//;
   if (scalar(@snaps)>0) {
    if ((time-$first)>$age) {
     $bb->color_line('red',"$type : Oldest snapshot taken ".ago(time-$first,1)." (Should be less than ".duration($age).")\n");
    } else {
     $bb->color_line('green',"$type : Oldest snapshot taken ".ago(time-$first,1)."\n");
    }
   }
   if ($acount > ($ecount*1.5)) {
    $bb->color_line('red',"$type : Expected $ecount snapshots but got $acount\n");
   } else {
    if ($acount < ($ecount*0.5)) {
     if ($acount < $ycount) {
      $bb->color_line('yellow',"$type : Expected $ecount snapshots but got $acount\n");
     } else {
      $bb->color_line('green',"$type : Expected $ycount snapshots but got $acount\n");
     }
    } else {
     $bb->color_line('green',"$type : Expected $ecount snapshots and got $acount\n");
    }
   }
  }
  my $commonname=$self->_getcommon();
  if ($commonname) {
   my $common=$self->_ageofcommon();
   if ($common<$commonage) {
    $bb->color_line('green',"common snapshot taken ".ago($common,1)."\n");
   } else {
    $bb->color_line('red',"common snapshot [".$self->_getcommon()."] taken ".ago($common,1)." (Should be less than ".duration($commonage).")\n");
   }
  } else {
   $bb->color_line('red',"No snapshots tagged with commonbackup\n");
  }
  # How many untagged snapshots are there?
  my $noholds=scalar(grep { scalar(keys(%{$self->{'_snapshots'}->{$_}->{'_holds'}}))==0 }  $self->snaplist());
  if ($noholds > $unheldmax) {
   $bb->color_line('red',"Found $noholds unheld snapshots (Should be less than $unheldmax)\n");
  } else {
   $bb->color_line('green',"Found $noholds unheld snapshots\n");
  }
  my $mytext=$bb->{'text'};
  $bb->{'text'}="&".$bb->{'color'}." ZFS <a name='$self->{'name'}'>".$self->{'name'}."</a>\n";
  # Now, for each child:
  my $childtext='';
  if ($recurse) {
   for my $c (keys(%{$self->{'_children'}})) {
    my $sub_bb=new SuperHobbit({test=>$c});
    $self->{'_children'}->{$c}->xymon($policy,$sub_bb,$recurse);
    $bb->color_line($sub_bb->{'color'},"<a href='#$c'>$c</a>\n");
 #print "result of $c is $sub_bb->{'color'}\n";
    $childtext.=$sub_bb->{'text'};
   }
  }
  $bb->{'text'}.=$mytext;
  $bb->{'text'}.=$childtext;
 #print "End xymon method for ".$self->{'name'}."\n";
}

sub _ageofcommon() {
 my $self=shift;
 my @commons=grep { scalar(grep(/commonbackup/, keys(%{$self->{'_snapshots'}->{$_}->{'_holds'}})))>0?$_:undef } $self->snaplist();
 my $common=$commons[0];
 $common=~s/.*@//;
 return time-$common;
}

sub _getcommon() {
 my $self=shift;
 my @commons=grep { scalar(grep(/commonbackup/, keys(%{$self->{'_snapshots'}->{$_}->{'_holds'}})))>0?$_:undef } $self->snaplist();
 return shift @commons;
}


1;
