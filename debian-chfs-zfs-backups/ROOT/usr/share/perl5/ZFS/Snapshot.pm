# To represent a ZFS object of type Snapshot
package ZFS::Snapshot;
use base ZFS::Dataset;
use ZFS::Tasklist;
use Array::Diff;

use strict;
use warnings;

sub new() {
 my $class=shift;
 my $self=shift || {};
 $self->{'_holds'}={} unless $self->{'_holds'};
 $self->{'_newholds'}={} unless $self->{'_newholds'};
 $self->{'_tasklist'}=new ZFS::Tasklist() unless $self->{'_tasklist'};
 bless $self, $class;
 return $self;
}

sub destroy() {
 my $self=shift;
 $self->{'_destroyed'}=1;
}

sub addHold() {
 my $self=shift;
 my $hold=shift;
 $self->{'_holds'}->{$hold}=1;
 $self->{'_newholds'}->{$hold}=1;
}

sub hashold() {
 my $self=shift;
 my $name=shift;
 return exists($self->{'_holds'}->{$name}) || exists($self->{'_newholds'}->{$name});
}

# Returns true if this snapshot requires a $tag $task-ing.
sub hastask() {
 my $self=shift;
 my $task=shift;
 my $tag=shift;
 my $result=$self->{'_tasklist'}->hastask($task,$tag);
 return $self->{'_tasklist'}->hastask($task,$tag);
}

sub addtag() {
 my $self=shift;
 my $tag=shift;
 $self->{'_newholds'}->{$tag}=1;
}

sub deltag() {
 my $self=shift;
 my $tag=shift;
 delete($self->{'_newholds'}->{$tag});
}

sub joblist() {
 my $self=shift;
 my @oldtags=sort keys(%{$self->{'_holds'}});
 my @newtags=sort keys(%{$self->{'_newholds'}});
 my $diff=Array::Diff->diff(\@oldtags,\@newtags);
 my @tasks=();
 push(@tasks,map { 'zfs hold '.$_.' '.$self->{'name'}    } @{$diff->added});
 push(@tasks,map { 'zfs release '.$_.' '.$self->{'name'} } @{$diff->deleted});
 push(@tasks, 'zfs destroy -d '.$self->{'name'}) if $self->{'_destroyed'};
 return @tasks;
}

sub appendtask() {
 my $self=shift;
 my $task=shift;
 # Simulate the task on myself
 $task->simulate($self);
 # add to my list of tasks
 $self->{'_tasklist'}->append($task);
}

sub addtask() {
 my $self=shift;
 my $action=shift;
 my $arg=shift;
 #print "Adding $action $arg to $self->{'name'} $self->{'host'}\n";
 $self->{'_tasklist'}->addtask($action,$arg,$self);
}

sub astimestamp() {
 my $self=shift;
 if ($self->{'name'} =~ /@(\d+)/) {
  return $1;
 } 
 return undef;
}

sub isdestroyed() {
 my $self=shift;
 return $self->{'_destroyed'};
}

sub actionsummary() {
 my $self=shift;
 my $hidenoop=shift;
 my $holds=join(',',sort keys(%{$self->{'_holds'}}));
 my $newholds=join(',',sort keys(%{$self->{'_newholds'}}));
 return if ($hidenoop and $holds eq $newholds and not $self->{'_destroyed'});
 return $self->{'name'}.' : '.$holds.' - '.$newholds.(($holds eq $newholds and not $self->{'_destroyed'})?' [Unchanged]':'').($self->{'_destroyed'}?' DESTROYED':'');
}

1;
