# To represent a single task
package ZFS::Task;

use strict;
use warnings;

sub new() {
 my $class=shift;
 my $self=shift || {};
 bless $self, $class;
 $self->{'_tasks'}=[] unless $self->{'_tasks'};
 return $self;
}
sub job() {
 my $self=shift;
 my $zfs=shift;
 return " $self->{'action'} $self->{'arg'} on $zfs\n";
}
sub simulate() {
 my $self=shift;
 my $zfs=shift;
 if ($self->{'action'} eq 'release') {
  delete($zfs->{'_newholds'}->{$self->{'arg'}}) if $zfs->{'_newholds'}->{$self->{'arg'}};
 } elsif ($self->{'action'} eq 'hold') {
  $zfs->{'_newholds'}->{$self->{'arg'}}=1;
 } elsif ($self->{'action'} eq 'destroy') {
  $zfs->{'_newholds'}={};
  $zfs->{'_destroyed'}=1;
 }
}
sub matches() {
 my $self=shift;
 my $task=shift;
 my $tag=shift;
 return $self->{'action'} eq $task && $self->{'arg'} eq $tag;
}

1;
