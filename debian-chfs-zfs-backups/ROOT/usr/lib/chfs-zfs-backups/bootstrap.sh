#!/bin/bash
# To bootstrap a ch-fileserver instance.
DOIT=false
while getopts ":hv-:" optchar; do
 case "${optchar}" in
   -)
       case "${OPTARG}" in
           really)
               DOIT=true
               ;;
           *)
               if [ "$OPTERR" = 1 ] && [ "${optspec:0:1}" != ":" ] ; then
                  echo "Unknown option --${OPTARG}" >&1
               fi
           ;;
       esac;;
   h)
       echo "Use: $0 --really to utterly and irrevokably trash your data" >&2
       exit 2
       ;;
   v)
       echo "Parsing option: '-${optchar}'" >&1
       ;;
   *)
       if [ "$OPTERR" != 1 ] || [ "${optspec:0:1}" = ":" ] ; then
           echo "Non-option argument: '-${OPTARG}'" >&2
       fi
       ;;
   esac
done
if [ $DOIT != true ] ; then
# Big Fat Warning
cat <<EOF
This script will:
 - Force DRBDS into primary mode, over-writing all data.
 - Create a ZPOOL from the DRBDS, destroying any data.
 - DESTROY ALL DATA ON DRBDS

Read that again. If you /really/ want to go ahead, you'll need to 
pass the '--really' argument. 
EOF
  exit 1
fi

# force all drbds to become primary
DRBDIDS=`cat /proc/drbd | grep cs: | grep Inconsistent | awk ' { print $1 } ' | sed s/://`
if [ ! -z "$DRBDIDS" ] ; then
 echo Destroying DRBDs
 for I in $DRBDIDS ; do
  drbdsetup /dev/drbd${I} primary --force
 done
else
 echo All DRBDS primary, skipping step
fi

if ! zpool list data >/dev/null 2>&1 ; then
 echo Creating zpool data
 zpool create data raidz2 /dev/drbd/by-res/*
else
 echo zpool data exists, skipping creation
fi

# Deliberately two steps to ensure we set a reservation if we are, for some reason,
# running this script when data/reserved already exists
zfs create data/reserved 
zfs set reservation=100M data/reserved

# Now we need to do some pump priming from the live fileserver
NOW=$(date +%s)
# instance number
INST=$(hostname -s | sed -e 's/^chfs//' -e 's/-c$//')
CHFS=ch-fileserver${INST}
ssh $CHFS zfs snapshot data/group@${NOW}
ssh $CHFS zfs snapshot data/config@${NOW}
ssh $CHFS zfs send -R data/group@${NOW} | zfs recv data/group
ssh $CHFS zfs send -R data/config@${NOW} | zfs recv data/config
